var Home_Page = function(){

    this.Weather = element(by.xpath("//a[@class='nav__link bg-hover-color'][contains(text(),'Weather')]"));
    this.CityName = element(by.xpath("//div[@class='form-group search-cities__block']//input[@id='q']"));
    this.SearchButton = element(by.xpath("//button[@class='btn btn-orange']"));
    this.AlertInvalidcity = element(by.xpath("//div[@class='alert alert-warning'][contains(text(),'Not found')]"));
    this.ValidCity = element(by.xpath("//a[contains(text(),'Bangalore, IN')]"));
    this.CurrentWheather = element(by.xpath("//span[@class='badge badge-info']"));
    this.Celcius =element(by.xpath("//span[@id='metric']"));
    this.Farenheit = element(by.xpath("//span[@id='imperial']"));
             
    this.enterCityName = function (name) 
    {
        this.CityName.sendKeys(name);
    };
         
    this.getInvalidcityText = function() {
        return this.AlertInvalidcity.getText();
    };
     
    this.clickSearch = function() {
        this.SearchButton.click();
        //return require('./navigate_page.js');//For navigate new page
    };

};
module.exports = new Home_Page();