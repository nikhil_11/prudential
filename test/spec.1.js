describe('admin Login', function() 
{
  beforeEach(function() {
    browser.waitForAngularEnabled(false);
    browser.ignoreSynchronization = true; // for non-angular websites
    browser.get('https://openweathermap.org/');
    browser.sleep(3000);
 });
 
 it('Find Links', function() 
    {     
      //find all links
      let linkCount = element.all(by.css('a'));

      // count links, with then() resolve promise and log count result
      linkCount.count().then(function(cnt){
          console.log('Total links = '+cnt);
        });
        
      //linkCount.click();
      browser.getTitle().then(function (PageTitle) {
        console.log('PageTitle=',PageTitle)
      })
    });

 var Home_Page = require('../Pages/Home_Page.1') //Through POM calling.
 it('Test for Invalid City', function()
    {
      Home_Page.enterCityName('Nikhil');
      Home_Page.SearchButton.click();  
      browser.sleep(2000);
      Home_Page.AlertInvalidcity.getText().then(function (text) {
       let t = text.split('×')[1];
       t = t.replace(/(\r\n|\n|\r)/gm, "");
       console.log(' City = ', t)
       expect(t).toEqual('Not found');
      }); 
    });  
 it('Test for Valid City', function()
    {
      Home_Page.enterCityName('Bangalore');
      Home_Page.SearchButton.click();  
      browser.sleep(2000);
      Home_Page.ValidCity.getText().then(function (City) {
        console.log('Successfully returns weather details for the city',City)
      });
      Home_Page.CurrentWheather.getText().then(function (Weather) {
        console.log('Current Weather: ',Weather)
      });
    });   
 it('Test for Valid City', function()
    {
      Home_Page.enterCityName('Bangalore');
      Home_Page.SearchButton.click();  
      browser.sleep(2000);
      Home_Page.ValidCity.getText().then(function (City) {
      console.log('Successfully returns weather details for the city',City)
      });
      Home_Page.CurrentWheather.getText().then(function (Weather) {
      console.log('Current Weather: ',Weather)
      });
      Home_Page.Farenheit.click();
      console.log('>>>>>>>>After select Farenheit<<<<<<<<');

      Home_Page.CurrentWheather.getText().then(function (Weather) {
      console.log('Current Weather: ',Weather);
      });
      console.log('>>>>>>>>After reload still results in celcius<<<<<<<<'); 
    });      
});