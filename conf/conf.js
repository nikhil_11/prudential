var Jasmine2HtmlReporter = require('c:/Protractor/node_modules/protractor-jasmine2-html-reporter');
var log4js = require('log4js');
var logger = log4js.getLogger();
logger.level = 'info';
logger.info("Some debug messages");

/* var log4js = require('log4js');
        log4js.configure('./config/log4js.json');
        //var logger = log4js.getLogger(); //for both console and file
        var log = log4js.getLogger("default");
        return log;
 */
      
exports.config = 
{
  framework: 'jasmine',
  seleniumAddress: 'http://localhost:4444/wd/hub/',
  directConnect: true,
  specs: ['..\\test\\spec.1.js'],

  multiCapabilities: 
  [{
    browserName: 'firefox'
  },
  {
    browserName: 'chrome'
  }],
  // Options to be passed to Jasmine-node.
  jasmineNodeOpts: {
    showColors: true, // Use colors in the command line report.
  },
   

  //Logs function
  "appenders": [{
    "type": "log4js-protractor-appender",
    "append": 'false',
    "maxLogSize": 20480,
    "backups": 3,
    "category": "relative-logger"
}], 

   // Screenshot function
   onPrepare: function() {
    browser.manage().window().maximize();
    browser.manage().timeouts().implicitlyWait(30000);
    jasmine.getEnv().addReporter(
      new Jasmine2HtmlReporter({
        savePath: '../target/screenshots',
        screenshotsFolder: 'AllScreenshots',
        takeScreenshots: true,
        takeScreenshotsOnlyOnFailures: false,
        cleanDestination: true,
        showPassed: true,
        fileName: 'Report',
        fileNameDateSuffix: true,

      })
    );
 }
   
}